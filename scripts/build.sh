#!/bin/bash

#Build within docker

TAG="16-alpine"
IMG="node:$TAG"

DIR="$(dirname "$0")"
ROOT_DIR="$(realpath "$DIR/..")"

if which docker &> /dev/null; then
  CMD=docker
elif which podman &> /dev/null; then
  CMD=podman
  IMG="docker.io/$IMG"
else
  echo "Unknown container runtime. Support docker & podman" >&2
  exit 1
fi

rm -rf "$ROOT_DIR/dist"

$CMD run --rm  --entrypoint "/bin/sh" -it -v "$ROOT_DIR:/app" "$IMG" -c 'cd /app && yarn install && npm run rollup'
