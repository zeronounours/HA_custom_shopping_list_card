#!/bin/bash


DIR="$(dirname "$0")"
ROOT_DIR="$DIR/.."
SOURCE_DIR="$ROOT_DIR/.sources"

declare -a MIRROR_DEPENDENCIES=(
  "@mdi/js"
  "home-assistant-js-websocket"
  "lit"
  "@material/mwc-textfield"
  "@material/mwc-button"
  "@material/mwc-list"
  "@material/mwc-select"
  "sortablejs"
  "superstruct"
)

declare -A COPY_FILES=(
  ["src/panels/lovelace/cards/hui-shopping-list-card.ts"]="src/hui-shopping-list-card.ts"
  ["src/panels/lovelace/cards/types.ts"]="src/types.ts"
  #["src/panels/lovelace/editor/config-elements/hui-shopping-list-editor.ts"]="src/hui-shopping-list-editor.ts"
  ["src/data/shopping-list.ts"]="src/data-shopping-list.ts"
  ["src/common/config/is_component_loaded.ts"]="src/common/config/is_component_loaded.ts"
  ["src/mixins/subscribe-mixin.ts"]="src/mixins/subscribe-mixin.ts"
)

# types to be imported from types.ts
declare -a TYPES_TO_IMPORT=(
  "SensorCardConfig"
  "ShoppingListCardConfig"
)

function clone_frontend_latest() {
  if [ -d "$SOURCE_DIR" ]; then
    (cd "$SOURCE_DIR" && git fetch)
  else
    git clone https://github.com/home-assistant/frontend.git "$SOURCE_DIR"
  fi
  # change to the latest tag
  (cd "$SOURCE_DIR" && git checkout "$(git tag | grep '^20' | sort | tail -n 1)")
}

function copy_sources() {
  for src in "${!COPY_FILES[@]}"; do
    dst="${COPY_FILES[$src]}"
    mkdir -p "$(dirname "$ROOT_DIR/$dst")"
    cp "$SOURCE_DIR/$src" "$ROOT_DIR/$dst"
  done
}

function update_dependencies() {
  # get new new dependencies versions
  local -A versions=()
  for dep in "${MIRROR_DEPENDENCIES[@]}"; do
    versions[$dep]="$(sed -rn '/"dependencies":/,/\},/{s#^.*"'"$dep"'":\s*"(.*)".*$#\1#p}' "$SOURCE_DIR/package.json")"
  done
  # update package.json
  cp "$ROOT_DIR/package.json" package.json.tmp
  for dep in "${!versions[@]}"; do
    sed -ri '/"dependencies":/,/\},/{s#^(.*"'"$dep"'":\s*").*(".*)$#\1'"${versions[$dep]}"'\2#}' package.json.tmp
  done
  mv package.json.tmp "$ROOT_DIR/package.json"
}

function filter_imported_types() {
  sed -rni '
    # keep the imports at the begining
    1 {
      p
      :a
      n
      /^export /!{p;b a}
    }
    # extract only wanted types
    :b
    /^export interface ('"$(IFS='|'; echo "${TYPES_TO_IMPORT[*]}")"') /{
      p
      :c
      n
      /^export /!{p;b c}
      b b
    }' "$ROOT_DIR/src/types.ts"
}

clone_frontend_latest && copy_sources && update_dependencies && filter_imported_types
