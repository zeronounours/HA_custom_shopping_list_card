import { HomeAssistant } from "custom-card-helpers";

export interface ShoppingListItem {
  id: number;
  name: string;
  unit: string;
  quantity: number;
  complete: boolean;
}

export const fetchItems = (hass: HomeAssistant): Promise<ShoppingListItem[]> =>
  hass.callWS({
    type: "shopping_list/items",
  });

export const updateItem = (
  hass: HomeAssistant,
  itemId: number,
  item: {
    name?: string;
    unit?: string;
    quantity?: number;
    complete?: boolean;
  }
): Promise<ShoppingListItem> =>
  hass.callWS({
    type: "shopping_list/items/update",
    item_id: itemId,
    ...item,
  });

export const clearItems = (hass: HomeAssistant): Promise<void> =>
  hass.callWS({
    type: "shopping_list/items/clear",
  });

export const addItem = (
  hass: HomeAssistant,
  name: string,
  unit?: string,
  quantity?: number,
): Promise<ShoppingListItem> =>
  hass.callWS({
    type: "shopping_list/items/add",
    name,
    unit,
    quantity,
  });

export const reorderItems = (
  hass: HomeAssistant,
  itemIds: [string]
): Promise<ShoppingListItem> =>
  hass.callWS({
    type: "shopping_list/items/reorder",
    item_ids: itemIds,
  });
