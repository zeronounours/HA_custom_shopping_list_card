import { TextFieldBase } from "@material/mwc-textfield/mwc-textfield-base";

// Fake interface for HaTextField (only contains used properties
export interface HaTextField extends TextFieldBase {
  invalid?: boolean;
  errorMessage?: string;
  icon?: boolean;
  iconTrailing?: boolean;
}
