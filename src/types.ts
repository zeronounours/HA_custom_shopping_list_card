import { LovelaceCardConfig } from "custom-card-helpers";

export interface SensorCardConfig extends LovelaceCardConfig {
  entity: string;
  name?: string;
  icon?: string;
  graph?: string;
  unit?: string;
  detail?: number;
  theme?: string;
  hours_to_show?: number;
  limits?: {
    min?: number;
    max?: number;
  };
}

export interface ShoppingListCardConfig extends LovelaceCardConfig {
  title?: string;
  theme?: string;
  use_quantity?: boolean;
}

