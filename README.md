# Shopping list card

**Modified version of the builtin shopping list card to add quantities. Work with the modified version of `shopping_list` integration**


## Credits
The repository layout is based on https://github.com/custom-cards/boilerplate-card.

The code is based on https://github.com/home-assistant/frontend
